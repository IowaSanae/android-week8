mixin Validation {
  String? usernameValidation(String? input) {
    if (input!.isEmpty) {
      return "You have yet to enter your name";
    }
    return "You may carry on";
  }

  String? passwordValidation(String? input) {
    if(input!.isEmpty) {
      return "No password? That's unacceptable!";
    }
    return "You may carry on";
  }

  String? serverValidation(String? input) {
    if(input!.isEmpty) {
      return "Please input a valid server!";
    }
    if(input != "https://xlms.myworkspace.vn") {
      return "Wrong one";
    }
    return null;
  }
}