import 'package:flutter/cupertino.dart';
import 'sakai_services.dart';

var sakaiUrl = "https://xlms.myworkspace.vn";

class SakaiHelper extends InheritedWidget {
  SakaiHelper({Key? key, required this.child})
      : super(key: key, child: child);

  final Widget child;
  final service = SakaiService(sakaiUrl: sakaiUrl);

  static SakaiHelper? of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<SakaiHelper>();
  }

  @override
  bool updateShouldNotify(SakaiHelper oldWidget) {
    return true;
  }
}
