import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:week8/mixins/mixin_validator.dart';
import '../sakai_helper.dart';

class XLMSLogin extends StatelessWidget {
  const XLMSLogin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "Login",
        home: Scaffold(
          appBar: AppBar(title: const Text("Login")),
          body: const XLMSLoginScreen(),
        ));
  }
}

class XLMSLoginScreen extends StatefulWidget {
  const XLMSLoginScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _XLMSLoginState();
  }
}

class _XLMSLoginState extends State<StatefulWidget> with Validation {
  final formKey = GlobalKey<FormState>();
  final email = TextEditingController();
  final password = TextEditingController();
  final server = TextEditingController(text: "https://xlms.myworkspace.vn");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            padding: const EdgeInsets.all(20.0),
            child: Form(
                key: formKey,
                child: Column(
                  children: [
                    TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      controller: email,
                      decoration: const InputDecoration(
                        icon: Icon(Icons.alternate_email),
                        labelText: "Email Address",
                        hintText: "abc@gmail.com",
                      ),
                      validator: usernameValidation,
                    ),
                    TextFormField(
                      obscureText: true,
                      controller: password,
                      decoration: const InputDecoration(
                        icon: Icon(Icons.password),
                        labelText: "Password",
                        hintText: "12345678",
                      ),
                      validator: passwordValidation,
                    ),
                    TextFormField(
                      keyboardType: TextInputType.url,
                      controller: server,
                      enabled: false,
                      decoration: const InputDecoration(
                        icon: Icon(Icons.link),
                        labelText: "Server",
                      ),
                      validator: serverValidation,
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 20),
                      child: TextButton(
                          style: TextButton.styleFrom(
                              backgroundColor: Colors.green,
                              primary: Colors.white),
                          onPressed: sakaiValidator,
                          child: const Text('Login')),
                    )
                  ],
                ))));
  }

  void sakaiValidator() async {
    var _helper = SakaiHelper.of(context);
    final form = formKey.currentState;

    if (!form!.validate()) {
      return;
    } else {
      final response = await _helper?.service.authenticate(email.text, password.text);

      print(response);
    }
  }

  @override
  void dispose() {
    email.dispose();
    password.dispose();
    server.dispose();
    super.dispose();
  }
}
